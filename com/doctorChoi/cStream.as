﻿package com.doctorChoi
{
	/*code by DoctorChoi (https://gitlab.com/DoctorChoi) */

	import flash.net.NetStream;
	import flash.net.NetConnection;
	import flash.events.NetStatusEvent;
	import flash.utils.ByteArray;
	import flash.events.EventDispatcher;
	import com.doctorChoi.cStreamEvent;
	//import 끗

	public class cStream extends EventDispatcher
	{
		public static const READY: String = "ready";
		public static const SUCCESS: String = "success";
		public static const DISCONNECT: String = "disconnect";
		public static const RECEIVE: String = "receive";
		public static const WARNING: String = "warning";
		public static const ERROR: String = "error";
		//상수선언 끗

		var _rm: String;
		var _nc: NetConnection;
		var _ss: NetStream;
		var _rs: NetStream;
		var _isCon: Boolean = false;
		var _http: String;
		var _cirrus: String;
		var _func: Function;
		//변수선언 끗

		public function cStream(_room: String, _key: String, _host: String = "p2p.rtmfp.net"): void
		{
			_rm = _room;
			_cirrus = _key;
			_http = "rtmfp://" + _host + "/" + _cirrus;
			_nc = new NetConnection();
			_nc.addEventListener(NetStatusEvent.NET_STATUS, _fnc);
			_nc.connect(_http);
		}
		//생성자 끗

		public function connect(fid: String): void
		{
			if (!_isCon)
			{
				_isCon = true;
				_rs = new NetStream(_nc, fid);
				_rs.addEventListener(NetStatusEvent.NET_STATUS, _fnc);
				var _tmp: Object = new Object();
				_tmp.receive = function (e)
				{
					_fmakeEvent(
					{
						type: RECEIVE,
						data: JSON.parse(e)
					});
				};
				_rs.client = _tmp;
				_rs.play(_rm);
			}
			else
			{
				return void;
			}
		}
		//연결하는 함수 끗

		public function get myPeerID(): String
		{
			return _nc.nearID;
		}
		//내 PeerID 반환하는 함수 끗

		public function get isConnected(): Boolean
		{
			return _isCon;
		}
		//연결되어있는지 보여주는 함수 끗

		public function disconnect(): void
		{
			if (_isCon)
			{
				_ss.close();
				_ss.removeEventListener(NetStatusEvent.NET_STATUS, _fnc);
				_rs.close();
				_rs.removeEventListener(NetStatusEvent.NET_STATUS, _fnc);
				_isCon = false;

				_ss = null;
				_rs = null;

				_fss();
			}
		}
		//연결 끊는 함수 끗

		public function set room(_room: String): void
		{
			if (!_isCon)
			{
				_rm = _room;
				try
				{
					disconnect();
				}
				catch (e)
				{

				};
			}
		}
		//새로운 방으로 바꾸는 함수 끗

		public function on(func: Function): void
		{
			if (_func == null)
			{
				_func = func;
				this.addEventListener(cStreamEvent.RECEIVE, _func);
			}
			else
			{
				throw new Error("이미 연결된 함수가 있음");
			}
		}
		//이벤트리스너 달아주는 함수 끗

		public function off(): void
		{
			if (_func != null)
			{
				this.removeEventListener(cStreamEvent.RECEIVE, _func);
				_func = null;
			}
			else
			{
				throw new Error("연결된 함수가 없음");
			}
		}
		//이벤트리스너 때주는 함수 끗

		public function send(info: String, data: Object): void
		{
			if (_isCon)
			{
				var _tmp: Object = new Object();
				_tmp.type = info;
				_tmp.data = data;
				_ss.send(RECEIVE, JSON.stringify(_tmp));
			}
		}
		//데이터 던지는 함수 끗

		private function _fnc(e: NetStatusEvent): void
		{
			if (e.info.level == "status")
			{
				switch (e.info.code)
				{
					case "NetConnection.Connect.Success":
						_fss();
						_fmakeEvent(
						{
							type: READY
						});
						break;
					case "NetStream.Connect.Success":
						_fmakeEvent(
						{
							type: SUCCESS
						});
						break;
					case "NetStream.Connect.Closed":
						_fmakeEvent(
						{
							type: DISCONNECT
						});
						break;
				}
			}
			else if (e.info.level == "warning")
			{
				_fmakeEvent(
				{
					type: WARNING
				});
			}
			else if (e.info.level == "error")
			{
				_fmakeEvent(
				{
					type: ERROR
				});
			}
		}
		//이벤트리스너용 함수 끗

		private function _fss(): void
		{
			_ss = new NetStream(_nc, NetStream.DIRECT_CONNECTIONS);
			_ss.addEventListener(NetStatusEvent.NET_STATUS, _fnc);
			_ss.publish(_rm);
			var _tmp: Object = new Object();
			_tmp.onPeerConnect = function (e: NetStream): Boolean
			{
				connect(e.farID);
				return true;
			};
			_ss.client = _tmp;
		}
		//보내는 스트림 설정하는 함수 끗

		private function _fmakeEvent(_dt: Object): void
		{
			dispatchEvent(new cStreamEvent(cStreamEvent.RECEIVE, _dt));
		}
		//이벤트 제조기 끗
	}
}